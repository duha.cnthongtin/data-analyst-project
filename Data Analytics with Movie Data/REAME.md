# Dataset

- Dữ liệu phim TMDB (được làm sạch từ dữ liệu gốc trên Kaggle)
- Tập dữ liệu này chứa thông tin về 10.000 phim được thu thập từ Cơ sở dữ liệu phim (TMDB), bao gồm xếp hạng và doanh thu của người dùng


## Thư viện sử dụng:

- Tất cả các mã được viết trên JupyterNoteBook. Các thư viện python bao gồm numpy, pandas, matplotlib, seaborn.
